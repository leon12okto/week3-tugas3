<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::GET('/pertanyaan','PertanyaanController@index');
Route::GET('/pertanyaan/create','PertanyaanController@create');
Route::POST('/pertanyaan','PertanyaanController@store');
Route::GET('/pertanyaan/{pertanyaan_id}','PertanyaanController@show');
Route::GET('/pertanyaan/{pertanyaan_id}/edit','PertanyaanController@edit');
Route::PUT('/pertanyaan/{pertanyaan_id}','PertanyaanController@update');
Route::DELETE('/pertanyaan/{pertanyaan_id}','PertanyaanController@destroy');